To invoke Apex classes to run at specific times, first implement the Schedulable interface for the class.
Then, schedule an instance of the class to run at a specific time using the System.schedule method

global class SomeClass implements Schedulable {
    global void execute(SchedulableContext ctx) {
        // awesome code here
    }
}
The parameter of this method is a SchedulableContext object. 
After a class has been scheduled, a CronTrigger object is created that represents the scheduled job.
It provides a getTriggerId method that returns the ID of a CronTrigger API object.

After you implement a class with the Schedulable interface, use the System.Schedule method to execute it. 
The System.Schedule method uses the user's timezone for the basis of all schedules,
but runs in system mode—all classes are executed, whether or not the user has permission to execute the class.
The System.Schedule method takes three arguments: a name for the job, a CRON expression used to represent the time and date the job is scheduled to run, and the name of the class

RemindOpptyOwners reminder = new RemindOpptyOwners();
// Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
String sch = '20 30 8 10 2 ?';
String jobID = System.schedule('Remind Opp Owners', sch, reminder);

You can also schedule a class using the user interface in the Apex Classes-Schedule Apex.
Just like with the other async methods we’ve covered so far, with Scheduled Apex you must also ensure that the scheduled job is finished before testing against the results. 
To do this, use startTest and stopTest again around the System.schedule method, to ensure processing finishes before continuing your test.

You can only have 100 scheduled Apex jobs at one time and there are maximum number of scheduled Apex executions per a 24-hour period.
Synchronous Web service callouts are not supported from scheduled Apex. 
To be able to make callouts, make an asynchronous callout by placing the callout in a method annotated with @future(callout=true) and call this method from scheduled Apex. 
Use extreme care if you’re planning to schedule a class from a trigger.
You must be able to guarantee that the trigger won’t add more scheduled classes than the limit. In particular, consider API 
bulk updates, import wizards, mass record changes through the user interface, and all cases where more than one record can be updated at a time.


global class DailyLeadProcessor implements Schedulable {
    global void execute(SchedulableContext sc){
        List<Lead> l = new List<Lead>();
        l=[select id, leadSource from lead where LeadSource=null limit 200];
        if(!l.isEmpty()){
        for( Lead ld : l){
            ld.LeadSource= 'Dreamforce';
           
        }
        update l;
        }
    }
}

//  TEST
@isTest
public class DailyLeadProcessorTest {
    //In the test class, insert 200 Lead records, schedule the DailyLeadProcessor class
    // to run and test that all Lead records were updated correctly.
    public static String CRON_EXP = '0 0 1 * * ?';
    static testmethod void testScheduledJob(){
        List<Lead> leads =new List<Lead>();
        for (Integer i=0; i<200; i++){
            Lead l = new Lead();
            l.LastName = 'Test'+ I;
            L.Company = 'TLT';
            l.Status = 'Open-Not Contacted';
            leads.add(l);
        }
        insert leads;   
        Test.startTest();
        String jobId= System.schedule('Scheduled Job', CRON_EXP, new DailyLeadProcessor());
        Test.stopTest();
        
    }
}

You can monitor the status of all jobs in the Salesforce user interface. From Setup, enter Jobs in the Quick Find box, then select Apex Jobs.

The Apex Jobs page shows all asynchronous Apex jobs with information about each job’s execution. 