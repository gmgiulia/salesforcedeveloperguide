An asynchronous process is a process or function that executes a task "in the background" without the user having to wait for the task to finish.
With asynchronous processing the user can get on with their work, the processing can be done in the background and the user can see the results at their convenience.

FUTURE METHODS	Run in their own thread, and do not start until resources are available(Web service callout), you use the @future annotation to identify methods that run asynchronously.

BATCH APEX Run large jobs that would exceed normal processing limits (Data cleansing or archiving of records).
Each time you invoke a batch class, the job is placed on the Apex job queue and is executed as a discrete transaction.


QUEUEABLE APEX Similar to future methods, but provide additional job chaining and allow more complex data types to be used (Performing sequential processing operations with external Web services).

SCHEDULED APEX Schedule Apex to run at a specified time (Daily or weekly tasks).

One of the main benefits of running asynchronous Apex is higher governor and execution limits.
For example, the number of SOQL queries is doubled from 100 to 200 queries when using asynchronous calls.

The platform uses a queue-based asynchronous processing framework. This framework is used to manage asynchronous requests for multiple organizations within each instance. The request lifecycle is made up of three parts:

Enqueue
The request gets put into the queue. This could be an Apex batch request, future Apex request or one of many others. The platform will enqueue requests along with the appropriate data to process that request.

Persistence
The enqueued request is persisted. Requests are stored in persistent storage for failure recovery and to provide transactional capabilities.

Dequeue
The enqueued request is removed from the queue and processed. Transaction management occurs in this step to assure messages are not lost if there is a processing failure.

Each request is processed by a handler. The handler is the code that performs functions for a specific request type.

@future: Future methods must be static methods, and can only return a void type.
The specified parameters must be primitive data types, arrays of primitive data types, or collections of primitive data types.
When using synchronous processing, all method calls are made from the same thread that is executing the Apex code, and no additional processing can occur until the process is complete. 
You can use future methods for any operation you’d like to run asynchronously.
The reason why objects can’t be passed as arguments to future methods is because the object can change between the time you call the method and the time that it actually executes. Remember, future methods are executed when system resources become available.
In this case, the future method may have an old object value when it actually executes
This provides the benefits of not blocking the user from performing other operations and providing higher governor and execution limits for the process.
Used for:
1-Callouts to external Web services.
2-Operations you want to run in their own thread, when time permits such as some sort of resource-intensive calculation or processing of records.
3-Isolating DML operations on different sObject types to prevent the mixed DML error.

global class newClass {
  @future
  public static void myFutureMethod(List<Id> myIds) {
    List<Account> accounts = [Select Id, Name from Account Where Id IN :myIdss];
    //write body here
    
  }
}
To test future methods, enclose your test code between the startTest and stopTest test methods. 
Test code cannot actually send callouts to external systems, so you’ll have to ‘mock’ the callout for test coverage.
@IsTest
private class Test_SMSUtils {
  @IsTest
  private static void testSendSms() {
    Test.setMock(HttpCalloutMock.class, new SMSCalloutMock());
    Test.startTest();
      SMSUtils.sendSMSAsync('111', '222', 'Greetings!');
    Test.stopTest();
    // runs callout and check results
    List<SMS_Log__c> logs = [select msg__c from SMS_Log__c];
    System.assertEquals(1, logs.size());
    System.assertEquals('success', logs[0].msg__c);
  }
}

SUMMARY
Methods with the future annotation must be static methods, and can only return a void type.
The specified parameters must be primitive data types, arrays of primitive data types, or collections of primitive data types; future methods can’t take objects as arguments.
Future methods won’t necessarily execute in the same order they are called. In addition, it’s possible that two future methods could run concurrently, which could result in record locking if the two methods were updating the same record.
Future methods can’t be used in Visualforce controllers in getMethodName(), setMethodName(), nor in the constructor.
You can’t call a future method from a future method. Nor can you invoke a trigger that calls a future method while running a future method. See the link in the Resources for preventing recursive future method calls.
The getContent() and getContentAsPDF() methods can’t be used in methods with the future annotation.
You’re limited to 50 future calls per Apex invocation, and there’s an additional limit on the number of calls in a 24-hour period. For more information on limits, see the link below.



CALLOUT
To make a Web service callout to an external service or API, you create an Apex class with a future method that is marked with (callout=true).
public class SMSUtils {
    // Call async from triggers, etc, where callouts are not permitted.
    @future(callout=true)
    public static void sendSMSAsync(String fromNbr, String toNbr, String m) {
        String results = sendSMS(fromNbr, toNbr, m);
        System.debug(results);
    }
    
    

public class AccountProcessor {
	@future
    public static void countContacts (Set<Id> accIds){
        //For each Account ID passed to the method, count the number of Contact records 
        //associated to it and update the 'Number_of_Contacts__c' field with this value.
        List<Account> accountId=[select id, Number_of_Contacts__c, (select id from contacts) from account where id IN :accIds];
        for(Account acc: accountId){
            List<Contact> cnt = acc.contacts;
            acc.Number_of_Contacts__c =cnt.size();
            
        }
        update accountId;
    }
}


@IsTest
public class AccountProcessorTest {
    public static testmethod void TestAccountProcessorTest() 
    {
        Account a = new Account();
        a.Name = 'Test Account';
        Insert a;

        Contact cont = New Contact();
        cont.FirstName ='Bob';
        cont.LastName ='Masters';
        cont.AccountId = a.Id;
        Insert cont;
        
        set<Id> setAccId = new Set<ID>();
        setAccId.add(a.id);
 
        Test.startTest();
            AccountProcessor.countContacts(setAccId);
        Test.stopTest();
        
        Account ACC = [select Number_of_Contacts__c from Account where id = :a.id LIMIT 1];
        System.assertEquals ( Integer.valueOf(ACC.Number_of_Contacts__c) ,1);
  }
  
}

BATCH APEX
Every transaction starts with a new set of governor limits, making it easier to ensure that your code stays within the governor execution limits.
If one batch fails to process successfully, all other successful batch transactions aren’t rolled back.
our class must implement the Database.Batchable interface and include the following three methods:

1-start: Used to collect the records or objects to be passed to the interface method execute for processing. 
The start method provides the collection of all records that the execute method will process in individual batches. 
This method is called once at the beginning of a Batch Apex job and returns either a Database.QueryLocator object or an Iterable that contains the records or objects passed to the job.

2-execute: Performs the actual processing for each chunk or “batch” of data passed to the method. The default batch size is 200 records. 
Batches of records are not guaranteed to execute in the order they are received from the start method.
When the job is complete, the finish method performs a query on the AsyncApexJob object (a table that lists information about batch jobs) to get the status of the job

3-finish: Used to execute post-processing operations (for example, sending an email) and is called once after all batches are processed.

global class MyBatchClass implements Database.Batchable<sObject> {
    global (Database.QueryLocator | Iterable<sObject>) start(Database.BatchableContext bc) {
        // collect the batches of records or objects to be passed to execute
        // returns either a Database.QueryLocator object or 
        // an Iterable that contains the records or objects passed to the job.
    }
    global void execute(Database.BatchableContext bc, List<P> records){
        // process each batch of records
    }    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations after all batches are processed
    }    
}

If you specify Database.Stateful in the class definition, you can maintain state across all transactions.
To invoke a batch class, simply instantiate it and then call Database.executeBatch
MyBatchClass myBatchObject = new MyBatchClass(); 
Id batchId = Database.executeBatch(myBatchObject);
You can also optionally pass a second scope parameter to specify the number of records that should be passed into the execute method for each batch
Id batchId = Database.executeBatch(myBatchObject, 100);

Each batch Apex invocation creates an AsyncApexJob record so that you can track the job’s progress. 
You can view the progress via SOQL or manage your job in the Apex Job Queue
AsyncApexJob job = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID = :batchId ];

f you specify Database.Stateful in the class definition, you can maintain state across all transactions. When using Database.Stateful, only instance member variables retain their values between transactions. 
Maintaining state is useful for counting or summarizing records as they’re processed.

To ensure fast execution of batch jobs, minimize Web service callout times and tune queries used in your batch Apex code. 
The longer the batch job executes, the more likely other queued jobs are delayed when many jobs are in the queue.

SUMMARY
You can only have five queued or active batch jobs at one time. 
You can evaluate your current count by viewing the Scheduled Jobs page in Salesforce or programmatically using SOAP API to query the AsyncApexJob object.
Batch jobs can also be programmatically scheduled to run at specific times using the Apex scheduler, or scheduled using the Schedule Apex page in the Salesforce user interface.
To use batch Apex, write an Apex class that implements the Salesforce-provided interface Database.Batchable and then invoke the class programmatically.

The Database.Batchable interface contains three methods that must be implemented.
1-start method:
global (Database.QueryLocator | Iterable<sObject>) start(Database.BatchableContext bc) {}
To collect the records or objects to pass to the interface method execute, call the start method at the beginning of a batch Apex job. 
This method returns either a Database.QueryLocator object or an iterable that contains the records or objects passed to the job.
When you’re using a simple query (SELECT) to generate the scope of objects in the batch job, use the Database.QueryLocator object. 

2-execute method:
global void execute(Database.BatchableContext BC, list<P>){}
This method is called for each batch of records that you pass to it.

This method takes -A reference to the Database.BatchableContext object.
-A list of sObjects, such as List<sObject>, or a list of parameterized types. 
If you are using a Database.QueryLocator, use the returned list.

3-finish method:
global void finish(Database.BatchableContext BC){}
To send confirmation emails or execute post-processing operations, it is called after all batches are processed.

global class LeadProcessor implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext Bc) {
        //  collect all Lead records in the org
        return Database.getQueryLocator([select id, LeadSource from Lead]);
    }
    global void execute(Database.BatchableContext bc, List<Lead> ll){
        //  update all Lead records in the org with the LeadSource value of 'Dreamforce'
        List<Lead> ld = new List<Lead>();
        for(Lead l : ll) {
            l.LeadSource = 'Dreamforce';
            ld.add(l);
        }
        update ld;
    }    
    global void finish(Database.BatchableContext bc){
        // execute any post-processing operations
        System.debug('Successful');
        AsyncApexJob job = [select id, status, numberOfErrors, JobItemsProcessed, TotalJobItems 
                            from AsyncApexJob where Id= :bc.getJobId()];
    }    
}

//test
@isTest
public class LeadProcessorTest {
	//insert 200 Lead records, execute the 'LeadProcessor' Batch class 
	@testSetup
    static void setup() {
        List<Lead> ld = new List<Lead>();
        for (Integer i=0; i<200; i++){
            ld.add(new Lead(FirstName='test' + i, lastname= 'tt', company= 'rt', Status='Open-not contacted'));
        }
        insert ld;
    }
    static testmethod void test(){
        Test.startTest();
     	LeadProcessor lp = new LeadProcessor();
        Id batchId= Database.executeBatch(lp);
        Test.stopTest();
        System.assertEquals(200, [select count() from lead where LeadSource = 'Dreamforce']);
    }
}