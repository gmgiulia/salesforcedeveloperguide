Apex - Classes - A class is a template or blueprint from which objects are created. An object is an instance of a class. This is the standard definition of Class.
A class can contain variables and methods. Variables are used to specify the state of an object, such as the object's Name or Type.
Methods are used to control behavior, such as getOtherQuotes or copyLineItems.
A class can contain other classes, exception types, and initialization code.
In Apex, you can define top-level classes (also called outer classes) as well as inner classes, that is, a class defined within another class. 
You can only have inner classes one level deep. 

public class topLevelClass {
   //code here
   classyInnerClass {
     // code here
   }
}

To define a class, you must specify:
1-Access modifiers:
private(method or variable is accessible only within the Apex class in which it is defined), protected(method or variable is visible to any inner classes in the defining Apex class, and to the classes that extend the defining Apex class), 
public(method or variable can be used by any Apex in this application or namespace), and global(method or variable can be used by any Apex code that has access to the class) in the declaration of a top-level class.
You do not have to use an access modifier in the declaration of an inner class.
Optional definition modifiers (such as virtual-The virtual definition modifier declares that this class allows extension and overrides. You cannot override a method with the override keyword unless the class has been defined as virtual.
, abstract-The abstract definition modifier declares that this class contains abstract methods, that is, methods that only have their signature declared and no body defined., )
2-The keyword class followed by the name of the class
Optional extensions and/or implementations

To DECLARE A VARIABLE, specify the following:
Optional: Access Modifiers
Required: The data type of the variable, such as String or Boolean.
Required: The name of the variable.
Optional: The value of the variable.

TO DEFINE A CONSTANT YOU NEED TO WRITE THE FINAL keyword:
public static final Decimal Salary; 
private final Integer age = 28;

To DEFINE A METHOD, specify the following:
Optional: Access Modifiers.
Required: The data type of the value returned by the method, use void if the method does not return a value.
Required: A list of input parameters for the method, separated by commas, each preceded by its data type, and enclosed in parentheses (). If there are no parameters, use a set of empty parentheses. Max is 32 input parameters.
Required: The body of the method, enclosed in braces {}. 


public class myFirstClass {       
    //class member variable
   public static Integer myAge = 28;  
   public static String myName = 'Julia'; 
   
   public static String whoAmI () {
   
      myJob = 'Salesforce Developer';
      return myName + myJob;
   }
}

SHARING 

Let us now discuss the different modes of sharing.

With Sharing
When the class will get executed, it will respect the User's access settings and profile permission. This is also called as the User mode.
The with sharing keyword allows you to specify that the sharing rules for the current user are considered for the class

Without Sharing
the Class is running in the System mode( User's access settings and profile permission won't be respected), ensure that the sharing rules for the current user are not enforced

Inherited sharing
Using inherited sharing enables you to pass AppExchange Security Review and ensure that your privileged Apex code is not used in unexpected or insecure ways. An Apex class with inherited sharing runs as with sharing when used as:
An Aura component controller
A Visualforce controller
An Apex REST service
Any other entry point to an Apex transaction

Virtual
this class can be extended and overrides are allowed. If the methods need to be overridden, then the classes should be declared with the virtual keyword.

Abstract
the class will only contain the signature of method and not the actual implementation.


CONSTRUCTOR
A constructor is code that is invoked when an object is created from the class blueprint. You do not need to write a constructor for every class. 
If a class does not have a user-defined constructor, a default, no-argument, public constructor is used.
After you write the constructor for a class, you must use the new keyword in order to instantiate an object from that class, using that constructor


public class TestObject {

   // Constructor with no arguments
   public TestObject() {
      // code here
  }
}


//Instantiated a new object of this type:
TestObject myTest = new TestObject();

In Apex, a constructor can be overloaded, that is, there can be more than one constructor for a class, each having different parameters.
public class Accounts {

  // First a no-argument constructor 
  public Accounts() {}

  // A constructor with one argument
  public Accounts (Boolean call) {}

  // A constructor with two arguments
  public Accounts (String email, Boolean call) {}

  // Though this constructor has the same arguments as the 
  // one above, they are in a different order, so this is legal
  public Accounts (Boolean call, String email) {}
}


HOW TO EXTEND A CLASS
A class that extends another class inherits all the methods and properties of the extended class. 
In addition, the extending class can override the existing virtual methods by using the override keyword in the method definition. 
A class extends another class using the extends keyword in the class definition, Remember a class can only extend one other class.

public virtual class FirstClass {
    public virtual void story() {
        System.debug('Writing some story');
    }

    public virtual Integer numberOfStories() {
        return 5;
    }
}

// Extension for the FirstClass class
public class SecondClass extends FirstClass{
    public override void story() {
        System.debug('Writing some text story with the SecondClass');
    } 
    // Method only in this class
    public String nameOfTheStory() {
        return 'Alice in Wonderland';
    }
}    
This code shows polymorphism.
It means that the behavior of a particular method (story)is different based on the object you’re calling it on(FirstClass or SecondClass).
Let's see how  to call the additional method (nameOfTheStory) on theSecondClass class.
SecondClass obj = new SecondClass();
// Call method specific to SecondClass only
String AnotherStory = obj.nameOfTheStory();

