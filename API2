Each resource in REST API is identified by a named Uniform Resource Identifier (URI) and is accessed using standard HTTP methods (HEAD, GET, POST, PATCH, DELETE).
REST API is based on the usage of resources, their URIs, and the links between them.

Salesforce workbench /services/data/v47.0/sobjects/case/describe
/services/data= Specifies that we’re making a REST API request
v47.0 = API version number
sobjects=Specifies that we’re accessing a resource under the sObject grouping
case= object being actioned
describe= action performed

POST Request Body:
{
"Name" : "Blackbeards Grog Emporium";
"Description" : "The finest grog in the seven seas."
}

SOAP API
Salesforce provides two SOAP API WSDLs(Web Services Description Language) for two different use cases. The enterprise WSDL is optimized for a single Salesforce org. 
It’s strongly typed, and it reflects your org’s specific configuration, meaning that two enterprise WSDL files generated from two different orgs contain different information.
The partner WSDL is optimized for use with many Salesforce orgs. It’s loosely typed, and it doesn’t change based on an org’s specific configuration.
If you’re writing an integration for a single Salesforce org, use the enterprise WSDL. For several orgs, use the partner WSDL.

Setup-API-Generate Enterprise WSDL: the enterprise WSDL reflects your org’s specific configuration, so whenever you make a metadata change to your org, regenerate the WSDL file.

SOAP and REST API use synchronous requests and are optimized for real-time client applications that update a few records at a time.

Bulk API is based on REST principles and is optimized for working with large sets of data. 
You can use it to insert, update, upsert, or delete many records asynchronously, meaning that you submit a request and come back for the results later.
Salesforce processes the request in the background.

STREAMING API
Streaming API is your radar. It lets you define events and push notifications to your client app when the events occur.
A PushTopic is an sObject that contains the criteria of events you want to listen to, such as data changes for a particular object.
You define the criteria as a SOQL query in the PushTopic and specify the record operations to notify on (create, update, delete, and undelete).
PushTopic queries support all custom objects and some of the popular standard objects, such as Account, Contact, and Opportunity.

PushTopic pushTopic = new PushTopic();
pushTopic.Name = 'AccountUpdates';
pushTopic.Query = 'SELECT Id, Name, Phone FROM Account WHERE BillingCity=\'San Francisco\'';
pushTopic.ApiVersion = 37.0;
insert pushTopic;

To set notification preferences explicitly, set the following properties to either true or false. By default, all values are set to true.

pushTopic.NotifyForOperationCreate = true;
pushTopic.NotifyForOperationUpdate = true;
pushTopic.NotifyForOperationUndelete = true;
pushTopic.NotifyForOperationDelete = true;

Making your Apex class available as a REST web service is straightforward. Define your class as global, and define methods as global static. 
Add annotations to the class and methods. For example, this sample Apex REST class uses one method. The getRecord method is a custom REST API call.
It’s annotated with @HttpGet and is invoked for a GET request.

@RestResource(urlMapping='/Account/*')
global with sharing class MyRestResource {
    @HttpGet
    global static Account getRecord() {
        // Add your code
    }
}

Making your Apex class available as a SOAP web service is as easy as with REST. Define your class as global. 
Add the webservice keyword and the static definition modifier to each method you want to expose. The webservice keyword provides global access to the method it is added to.

global with sharing class MySOAPWebService {
    webservice static Account getRecord(String id) {
        // Add your code
    }
}


@RestResource(urlMapping='/Accounts/*/contacts')
global with sharing class AccountManager {

    @HttpGet
    global static Account getAccount() {
        RestRequest request = RestContext.request;
        // grab the accountId from the end of the URL
        String accountId = request.requestURI.substringBetween('Accounts/', '/contacts');
        Account result = [SELECT ID,Name,(SELECT ID,Name FROM Contacts) FROM Account WHERE Id = :accountId ];
        system.debug(result);
        return result;
        
    }
}


@IsTest
private class AccountManagerTest {

    @isTest static void testGetAccount() {
        Id recordId = createTestRecord();
        // Set up a test request
        RestRequest request = new RestRequest();
        request.requestUri =
            'https://juliadev-dev-ed.my.salesforce.com/services/apexrest/Accounts/'+ recordId+'/contacts';
        request.httpMethod = 'GET';
        RestContext.request = request;
        // Call the method to test
        Account thisAccount = AccountManager.getAccount();
        // Verify results
        System.assert(thisAccount != null);
        System.assertEquals('Test record', thisAccount.Name);
    }

   

    // Helper method
    static Id createTestRecord() {
        // Create test record
        Account accountTest = new Account(
        Name='Test record');
        insert accountTest;
       
        Contact TestCon= new Contact(
            LastName='Test', 
            AccountId = accountTest.id);
        return accountTest.Id;
    }          

}

