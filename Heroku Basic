Heroku is a great place to run apps and microservices that you can use with Salesforce through a variety of integration methods.

Four common reasons to integrate apps on Heroku with Salesforce are:

Data replication- copying or synchronizing data between Salesforce and another system
Data proxies-Data proxies aggregate different datastores, but unlike data replication, the data isn't copied. The data can be read only on demand.
Custom user interfaces-they can run on Heroku and be integrated into the Salesforce UI or just with Salesforce data.
External processes-External processes can offload batch processing or workflow and trigger event handling to apps on Heroku

A number of methods to accomplish these types of integrations are available, including:

Heroku Connect-Heroku Connect provides both data replication and data proxies for Salesforce. Data replication synchronizes data between Salesforce and a Heroku Postgres database. Depending on how it's configured, the synchronization is either one way or bidirectional. 
Salesforce Connect-data proxy to pull OData or other data sources into Salesforce on demand
Salesforce REST APIs-access to Salesforce data through simple JSON-formatted HTTP requests
Callouts-to call external processes on Heroku
Canvas-load an external user interface into Salesforce that can interact with Salesforce data through a JavaScript API

RECAP:
To replicate data between Salesforce and Heroku, use Heroku Connect.
To expose a Heroku Postgres database to Salesforce, use Heroku Connect External Object.
To proxy OData, SOAP, XML, or JSON data sources into Salesforce, use Salesforce Connect.
If Heroku Connect doesn’t fit the bill, like when you have a custom UI on Heroku where users log in via Salesforce, use the Salesforce REST APIs.
To offload or extend the processing of Salesforce data events, use callouts from Salesforce to Heroku.
To embed a custom UI from Heroku (or other external web app) into Salesforce, use Canvas.


Data replication with Heroku Connect can be one way, from Salesforce to Heroku Postgres, or bidirectional. 
Data replication from Heroku Postgres to Salesforce can be configured to execute with very low latency, albeit not in real time.

To configure Heroku Connect, you authenticate to a Salesforce instance using OAuth and allow Heroku Connect to make API calls on your behalf. 
Heroku Connect uses this API connection to synchronize the data between Salesforce and the Heroku Postgres database.
