In this repository you can find all Salesforce Guides for Developers.

If you want a "funnier" understanding on how to become a Salesforce Developer start
with this trailmix:

https://trailhead.salesforce.com/users/julia91/trailmixes/salesforce-developer-basic-trail

If you just want to learn APEX language click here:

https://trailhead.salesforce.com/users/julia91/trailmixes/learn-apex

Learn API:

https://trailhead.salesforce.com/users/julia91/trailmixes/learn-api